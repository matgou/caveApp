import { NavController, NavParams, AlertController } from 'ionic-angular';

export class abstractPage {
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController) {
  }

  goToPage(pageName:string) {
    this.navCtrl.setRoot(pageName);
  }
  pushPage(pageName:string) {
    this.navCtrl.push(pageName);
  }
  goBack() {
    this.navCtrl.pop();
  }

  isPageActive() {
    return 'secondary';
  }
  
  getActivePage(): string {
    return this.navCtrl.getActive().name;
  }

  showError(error) {
    console.log("Error in list-wine.ts");
    console.log(error);
    let alert = this.alertCtrl.create({
      title: 'Erreur',
      subTitle: error,
        buttons: ['Fermer']
      });
      alert.present();
  }
}
