import { Component } from '@angular/core';
import { Platform,NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ViewChild } from '@angular/core';
import { LoginPage } from '../pages/login/login';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  @ViewChild('content') navCtrl: NavController

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public auth: AuthServiceProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  getUserName() {
    let info = this.auth.getUserInfo();
    if(info !== undefined) {
      return this.capitalizeFirstLetter(info.role);
    }
    return 'Utilisateur';
  }
  goToRoot() {
    this.navCtrl.first();
  }
  goToPage(pageName:string) {
    this.navCtrl.setRoot(pageName);
  }
  getActivePage(): string {
    if(this.navCtrl.getActive() !== undefined) {
      return this.navCtrl.getActive().name;
    }
    return '';
  }
  pushPage(pageName:string) {
    this.navCtrl.push(pageName);
  }
  goBack() {
    this.navCtrl.pop();
  }
  logout() {
    this.auth.logout();
    this.goToPage("LoginPage");
  }
}
