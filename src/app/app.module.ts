import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { LoginPageModule } from '../pages/login/login.module';
import { HomePage } from '../pages/home/home';
import { WineDataProvider } from '../providers/wine-data/wine-data';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { HttpModule } from '@angular/http';
import { GuidProvider } from '../providers/guid/guid';
import { Camera } from '@ionic-native/camera';
import { CategorieDataProvider } from '../providers/categorie-data/categorie-data';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';//import in app.module.ts

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    LoginPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    WineDataProvider,
    GuidProvider,
    CategorieDataProvider,
    AuthServiceProvider,
  ]
})
export class AppModule {}
