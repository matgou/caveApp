import { Http, RequestOptions, Headers } from '@angular/http';
import { GuidProvider } from '../providers/guid/guid';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { NavController } from 'ionic-angular';

/**
 * restConnector
 */
export class restConnector {
  private BASE_URL="http://caveapp.kapable.info";
  protected objectType:string;

  constructor(private httpClient: Http, private UUIDGenerator: GuidProvider, private auth: AuthServiceProvider, private navCtrl: NavController) {
    console.log('restConnector : constructor');
  }

  private getOptions(authorizationMandatory?:boolean) {
    if(authorizationMandatory === undefined) {
      authorizationMandatory=true;
    }
    let headers = new Headers({'Content-Type': 'application/json'});
    if(this.auth !== null) {
      headers.append('Authorization','Bearer ' + this.auth.getToken());
    } else if(authorizationMandatory == true) {
      if(this.navCtrl !== null) {
        this.navCtrl.setRoot("LoginPage");
      }
    }
    return new RequestOptions({headers: headers});
  }

  private buildUrl(objectType:string, filter:string) {
      return this.BASE_URL + '/' + objectType + '?' + filter;
  }

  public _save(objectType:string, updatedObject:any): Observable<{}> {
    console.log("Send request to save " + objectType);
    return this.httpClient.patch(this.buildUrl(objectType,'id=eq.' + updatedObject.id), updatedObject, this.getOptions()).pipe(
      catchError(this.handleError)
    );
  }

  public _push(objectType:string, object:any): Observable<{}> {
    console.log("Send post request to " + objectType);
    if(object.id == undefined) {
      let guid = this.UUIDGenerator.newGuid();
      console.log("generate guid: " + guid)
      object.id = guid;
    }

    return this.httpClient.post(this.buildUrl(objectType,''), object, this.getOptions()).pipe(
      catchError(this.handleError)
    );
  }


  public _find(objectType:string, filter:string) {
    console.log("Get object (" + objectType + ") where : " + filter);
    return this.httpClient.get(this.buildUrl(objectType,filter), this.getOptions()).map(res => res.json()).pipe(
      catchError(this.handleError)
    );
  }

  public _rpc(functionName:string) {
    return this.httpClient.get(this.BASE_URL + '/rpc/' + functionName, this.getOptions()).map(res => res.json());
  }

  public __rpc(functionName:string, object:any) {
    return this.httpClient.post(this.BASE_URL + '/rpc/' + functionName, object, this.getOptions()).map(res => res.json());
  }

  public _getByFilter(objectType:string, filter:string) {
    let object = Observable.create((observer) => {
      // Call
      let response = this.httpClient.get(this.buildUrl(objectType, filter), this.getOptions()).map(res => res.json());
      response.subscribe(
        (data) => {
          if(data.length == 1) {
            observer.next(data[0]);
          } else {
            observer.error("Unable to find uniq object whith key : " + filter);
          }
        },
        (error) => { observer.error(error); }
      );
    });
    return object;
  }

  public _get(objectType:string, objectId:string) {
    return this._getByFilter(objectType, 'id=eq.' + objectId);
  }

  protected handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    let object = Observable.create((observer) => {
      observer.error('Une erreur s\'est produite, réessayez plus tard.');
    });
    return object;
  };
}
