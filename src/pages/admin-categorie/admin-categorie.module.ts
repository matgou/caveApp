import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminCategoriePage } from './admin-categorie';

@NgModule({
  declarations: [
    AdminCategoriePage,
  ],
  imports: [
    IonicPageModule.forChild(AdminCategoriePage),
  ],
})
export class AdminCategoriePageModule {}
