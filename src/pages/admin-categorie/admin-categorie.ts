import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { abstractPage } from '../../app/abstractPage';
import { CategorieDataProvider } from '../../providers/categorie-data/categorie-data';

/**
 * Generated class for the AdminCategoriePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-admin-categorie',
  templateUrl: 'admin-categorie.html',
})
export class AdminCategoriePage extends abstractPage {

  public categories;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public categorieDataProvider: CategorieDataProvider
  ) {
    super(navCtrl, navParams, alertCtrl);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminCategoriePage');
    this.reloadList();
  }

  reloadList() {
    this.categorieDataProvider.getAll().subscribe(
      (categories) => { this.categories = categories; },
      (error) => { this.showError(error); }
    );
  }

  showError(error) {
  console.log("Error in edit-wine.ts");
  console.log(error);
  let alert = this.alertCtrl.create({
    title: 'Erreur',
    subTitle: error,
      buttons: ['Fermer']
    });
    alert.present();
  }




  edit(idCategorie, nomCategorie) {

    let alert=this.alertCtrl.create({
    title: 'Edition Categorie',
    inputs: [
      {
        name: 'label',
        placeholder: 'Categorie',
        value: nomCategorie,
      }
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Modifier',
        handler: data => {
          this.categorieDataProvider.save({ 'id': idCategorie, 'name': data.label }).subscribe(
            (success) => {
              console.log("categorie created");
              this.reloadList();
            },
            (error) => { this.showError(error); }
          );
        }
      }
    ]
    });
    alert.present();
  }

  add() {
    let alert=this.alertCtrl.create({
    title: 'Nouvelle Categorie',
    inputs: [
      {
        name: 'label',
        placeholder: 'Categorie'
      }
    ],
    buttons: [
      {
        text: 'Annuler',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Créer',
        handler: data => {
          this.categorieDataProvider.push({ 'name': data.label }).subscribe(
            (success) => {
              console.log("categorie created");
              this.reloadList();
            },
            (error) => { this.showError(error); }
          );
        }
      }
    ]
  });
  alert.present();
  }
}
