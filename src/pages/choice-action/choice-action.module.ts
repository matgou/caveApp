import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChoiceActionPage } from './choice-action';

@NgModule({
  declarations: [
    ChoiceActionPage,
  ],
  imports: [
    IonicPageModule.forChild(ChoiceActionPage),
  ],
})
export class ChoiceActionPageModule {}
