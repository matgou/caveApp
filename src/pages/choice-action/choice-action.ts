import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { abstractPage } from '../../app/abstractPage';
import { WineDataProvider } from '../../providers/wine-data/wine-data';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the ChoiceActionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-choice-action',
  templateUrl: 'choice-action.html',
})
export class ChoiceActionPage extends abstractPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public barcodeScanner: BarcodeScanner,
    public wineDataProvider: WineDataProvider) {
      super(navCtrl, navParams, alertCtrl);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoiceActionPage');
  }

  scan() {
    this.barcodeScanner.scan().then(barcodeData => {
     console.log('Barcode data : ', barcodeData.text);
     this.wineDataProvider.getByBarcode(barcodeData.text).subscribe(
       (wine:any) => {

         console.log('Redirect to view :');
         console.log(wine);
         //let wine = response;
         this.navCtrl.push("ViewWinePage", {id: wine.id});
       },
       (error) => {
         console.log(error);
         this.navCtrl.push("EditWinePage", { barcode: barcodeData.text });
       }
     );
    }).catch(err => {
      console.log('Error', err);
    });
  }

}
