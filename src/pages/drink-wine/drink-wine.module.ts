import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrinkWinePage } from './drink-wine';

@NgModule({
  declarations: [
    DrinkWinePage,
  ],
  imports: [
    IonicPageModule.forChild(DrinkWinePage),
  ],
})
export class DrinkWinePageModule {}
