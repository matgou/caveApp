import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { abstractPage } from '../../app/abstractPage';
import { WineDataProvider } from '../../providers/wine-data/wine-data';

/**
 * Generated class for the DrinkWinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-drink-wine',
  templateUrl: 'drink-wine.html',
})
export class DrinkWinePage extends abstractPage {
  public id: number;
  public wine:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public wineDataProvider: WineDataProvider) {
    super(navCtrl, navParams, alertCtrl);
    this.id = navParams.get('id');
    wineDataProvider.get(this.id).subscribe(
      (wine) => { this.wine = wine; }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DrinkWinePage');
  }

  drink(quantity:number) {
    this.wine.stock = this.wine.stock - quantity;
    if(this.wine.stock < 0) {
      this.wine.stock = 0;
    }
    this.wineDataProvider.save(this.wine).subscribe(
      (result) => {
        console.log("success to save vin")
        this.navCtrl.pop();
      }
    );
  }

}
