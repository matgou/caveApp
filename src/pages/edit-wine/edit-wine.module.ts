import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditWinePage } from './edit-wine';

@NgModule({
  declarations: [
    EditWinePage,
  ],
  imports: [
    IonicPageModule.forChild(EditWinePage),
  ],
})
export class EditWinePageModule {}
