import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListWinePage } from './list-wine';

@NgModule({
  declarations: [
    ListWinePage,
  ],
  imports: [
    IonicPageModule.forChild(ListWinePage),
  ],
})
export class ListWinePageModule {}
