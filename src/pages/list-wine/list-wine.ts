import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { abstractPage } from '../../app/abstractPage';
import { WineDataProvider } from '../../providers/wine-data/wine-data';
import { ActionSheetController } from 'ionic-angular';
import { DomSanitizer  } from '@angular/platform-browser';

/**
 * Generated class for the ListWinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list-wine',
  templateUrl: 'list-wine.html',
})
export class ListWinePage extends abstractPage {
  public wines: any;

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public alertCtrl: AlertController,
     public wineDataProvider: WineDataProvider,
     private sanitizer: DomSanitizer,
     public actionSheetCtrl: ActionSheetController) {
    super(navCtrl, navParams, alertCtrl);

  }

  presentActionSheet(wine) {
    let actionSheet = this.actionSheetCtrl.create({
      title: wine.name,
      buttons: [
        {
          text: 'Voir les informations',
          handler: () => {
            this.viewWine(wine);
          }
        },{
          text: 'Boire',
          handler: () => {
            this.drink(wine);
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListWinePage');
    this.setItems();
  }

  setItems() {
    this.wineDataProvider.getActifJoinCategorieColor().subscribe(
      (response) => { this.wines = response; },
      (error) => { this.showError(error); }
    );
  }

  toRgba(color:string) {
    let style='color: rgba(' + color + ', 1);';
    return this.sanitizer.bypassSecurityTrustStyle(style);
  }

  filterItems(ev: any) {
    this.wineDataProvider.getActifJoinCategorieColor().subscribe(
      (response) => {
        this.wines = response;
        let val = ev.target.value;
        if (val && val.trim() !== '') {
          this.wines = this.wines.filter(function(item) {
            return item.name.toLowerCase().includes(val.toLowerCase());
          });
        }
      },
      (error) => { this.showError(error); }
    );
  }

  more(wine) {
    console.log(wine);
    console.log("More information about" + wine.name);
    this.presentActionSheet(wine);
  }

  edit(wine) {
    console.log("Edit information about" + wine.name);
    this.navCtrl.push("EditWinePage", {id: wine.id});
  }

  drink(wine) {
    this.navCtrl.push("DrinkWinePage", {id: wine.id});
  }

  viewWine(wine) {
    console.log("view wine id=" + wine.id);
    this.navCtrl.push("ViewWinePage", {id: wine.id});
  }
}
