import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewWinePage } from './view-wine';

@NgModule({
  declarations: [
    ViewWinePage,
  ],
  imports: [
    IonicPageModule.forChild(ViewWinePage),
  ],
})
export class ViewWinePageModule {}
