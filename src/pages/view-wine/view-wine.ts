import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { abstractPage } from '../../app/abstractPage';
import { WineDataProvider } from '../../providers/wine-data/wine-data';
import { CategorieDataProvider } from '../../providers/categorie-data/categorie-data';

/**
 * Generated class for the ViewWinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-wine',
  templateUrl: 'view-wine.html',
})
export class ViewWinePage extends abstractPage {
  public id:number;
  public wine:any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public wineDataProvider: WineDataProvider,
    public categorieDataProvider: CategorieDataProvider,
    public alertCtrl: AlertController) {
    super(navCtrl, navParams, alertCtrl);
    this.id = navParams.get('id');
    console.log(this.id);
    wineDataProvider.get(this.id).subscribe(
      (data) => {
          this.wine = data;
          console.log(this.wine);
          categorieDataProvider.get(this.wine.id_categorie).subscribe(
            (data) => { this.wine.categorie = data }
          );
          console.log(this.wine);
        }
    );
  }


  trash(wine) {
    const prompt = this.alertCtrl.create({
      title: 'Suppression d\'un vin',
      message: "Confirmer que vous souhaiter supprimer un vin",
      buttons: [
        {
          text: 'Annuler',
          handler: data => {
            console.log('Annuler clicked');
          }
        },
        {
          text: 'Supprimer',
          handler: data => {
            console.log('Supprimer clicked');
            console.log("Set wine state to '1'");
            wine.state = 1;

            delete wine.categorie;
            this.wineDataProvider.save(wine).subscribe(
              (success) => {
                console.log("wine deleted")
                this.goToPage('ListWinePage');
              },
              (error) => {
                this.showError(error); }
              );
            }
          }

      ]
    });
    prompt.present();
  }


  showError(error) {
    console.log("Error in edit-wine.ts");
    console.log(error);
    let alert = this.alertCtrl.create({
      title: 'Erreur',
      subTitle: error,
        buttons: ['Fermer']
      });
      alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewWinePage');
  }

  edit(wine) {
    console.log("Edit information about" + wine.name);
    this.navCtrl.push("EditWinePage", {id: wine.id});
  }

  drink(wine) {
    this.navCtrl.push("DrinkWinePage", {id: wine.id});
  }

}
