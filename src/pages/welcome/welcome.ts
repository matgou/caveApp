import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { abstractPage } from '../../app/abstractPage';
import { Chart } from 'chart.js';
import { WineDataProvider } from '../../providers/wine-data/wine-data';
import {Platform} from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

/**
 * Generated class for the WelcomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage extends abstractPage {
  @ViewChild('pieCanvas') pieCanvas;

  pieChart: any;
  countBottle: number = -1;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl:AlertController,
    public wineDataProvider: WineDataProvider,
    public platform: Platform,
    public auth: AuthServiceProvider) {
    super(navCtrl, navParams, alertCtrl);
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  getUserName() {
    let info = this.auth.getUserInfo();
    if(info !== undefined) {
      return this.capitalizeFirstLetter(info.role);
    }
    return 'Utilisateur';
  }

  toChartData(response) {
    this.countBottle=0;
    let dataObject = {
      'labels': [],
      'data': [],
      'backgroundColor': []
    };

    for(let stat of response) {
      this.countBottle = stat.countbottle + this.countBottle;
      dataObject.labels.push(stat.categorie);
      dataObject.data.push(stat.countbottle);
      dataObject.backgroundColor.push('rgba(' + stat.color + ', 0.2)');
    }
    return dataObject;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
    this.platform.ready().then((readySource) => {
      console.log('Width: ' + this.platform.width());
      console.log('Height: ' + this.platform.height());
      this.pieCanvas.nativeElement.height=this.platform.width();

      this.wineDataProvider.statCategorie().subscribe((response) => {
        let chartData = this.toChartData(response);
        this.pieChart = new Chart(this.pieCanvas.nativeElement, {

          type: 'pie',
          data: {
            labels: chartData.labels,
            datasets: [{
                label: '# de bouteilles',
                data: chartData.data,
                backgroundColor: chartData.backgroundColor
            }],
          },
          options: {
					  responsive: true,
            maintainAspectRatio: false,
					   legend: {
               display:true,
					     position: 'bottom'
					   },
             layout: {
               padding: {
                 left: 0,
                 right: 0,
                 top: 0,
                 bottom: 0
               }
             }
          }
        });
      });
    });
  }

}
