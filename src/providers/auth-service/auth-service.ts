import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { GuidProvider } from '../guid/guid';
import 'rxjs/add/operator/map';
import { restConnector } from '../../app/rest-connector';
import * as JWT from 'jwt-decode';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthServiceProvider extends restConnector {
  user:any;
  token:string;

  constructor(public http: Http, guidProvider: GuidProvider) {
    super(http, guidProvider, null, null);
    console.log('Hello AuthServiceProvider Provider');
  }

  public login(credentials) {
    if (credentials.email === null || credentials.pass === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {
        let access=false;
        this.__rpc('login', credentials).subscribe(
          (success) => {
            console.log('Login success');
            console.log(success);
            this.token = success[0].token;
            this.user = JWT(this.token);
            console.log(this.user);
            access=true;
            observer.next(access);
            observer.complete();
          },
          (error) => {
            console.log('Login Error : ');
            console.log(error);
            observer.next(access);
            observer.complete();
          }
        )
      });
    }
  }

  public register(credentials) {
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      // At this point store the credentials to your backend!
      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }

  public getUserInfo() {
    return this.user;
  }

  public getToken() {
    return this.token;
  }

  public logout() {
    return Observable.create(observer => {
      this.token = null;
      observer.next(true);
      observer.complete();
    });
  }
}
