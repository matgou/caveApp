import { Injectable } from '@angular/core';
import { GuidProvider } from '../guid/guid';
import { restConnector } from '../../app/rest-connector';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { AuthServiceProvider } from '../auth-service/auth-service';

/*
  Generated class for the CategorieDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategorieDataProvider extends restConnector {

  constructor(http: Http, guidProvider: GuidProvider, auth: AuthServiceProvider) {
    super(http, guidProvider, auth, null);
    this.objectType = 'categorie';

    console.log('WineDataProvider : constructor');
  }

  public getAll(): Observable<{}> {
    return this._find(this.objectType, '');
  }

  public get(objectId): Observable<{}> {
    return this._get(this.objectType,objectId);
  }

  public save(updatedObject): Observable<{}> {
    return this._save(this.objectType, updatedObject);
  }

  public push(object): Observable<{}> {
    return this._push(this.objectType, object);
  }
}
