import { Injectable } from '@angular/core';

/*
  Generated class for the GuidProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GuidProvider {

  constructor() {
    console.log('Hello GuidProvider Provider');
  }

  // http://stackoverflow.com/questions/26501688/a-typescript-guid-class
  newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : ( r & 0x3 | 0x8 );
        return v.toString(16);
    });
  }
}
