import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { GuidProvider } from '../guid/guid';
import { restConnector } from '../../app/rest-connector';
import { AuthServiceProvider } from '../auth-service/auth-service';

/*
  Generated class for the WineDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WineDataProvider extends restConnector {

  constructor(http: Http, guidProvider: GuidProvider, auth: AuthServiceProvider) {
    super(http, guidProvider, auth, null);
    this.objectType = 'vin';

    console.log('WineDataProvider : constructor');
  }

  public getAll(): Observable<{}> {
    return this._find(this.objectType, '');
  }

  public get(objectId): Observable<{}> {
    return this._get(this.objectType,objectId);
  }

  public save(updatedObject): Observable<{}> {
    return this._save(this.objectType, updatedObject);
  }

  public push(object): Observable<{}> {
    return this._push(this.objectType, object);
  }

  /*******************************************************/
  /*  CUSTOM Request                                     */
  /*******************************************************/
  public getActif(): Observable<{}> {
    return this._find('vin', 'state=eq.0');
  }

  public countBottleInStock(): Observable<{}> {
    console.log("Call remote procedure : countBottleInStock");
    return this._rpc("countbottleinstock");
  }

  public getActifJoinCategorieColor(): Observable<{}> {
    console.log("Call remote procedure : actifjoincategoriecolor");
    return this._rpc("actifjoincategoriecolor");
  }


  public statCategorie(): Observable<{}> {
    console.log("Call remote procedure : statcategorie");
    return this._rpc("statcategorie");
  }

  public getByBarcode(barcode:string): Observable<{}> {
    return this._getByFilter(this.objectType, 'barcode=eq.' + barcode);
  }
}
